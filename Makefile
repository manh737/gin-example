createdb:
	docker exec -it postgres12-database createdb --username=root --owner=root simple_bank
dropdb:
	docker exec -it postgres12-database dropdb simple_bank
postgres:
	docker run --name postgres12-database -p 5432:5432 -e POSTGRES_USER=root -e POSTGRES_PASSWORD=secret -d postgres:12-alpine
migrateup:
	migrate -path db/migrations -database "postgresql://root:secret@localhost:5432/simple_bank?sslmode=disable" -verbose up
migratedown:
	migrate -path db/migrations -database "postgresql://root:secret@localhost:5432/simple_bank?sslmode=disable" -verbose down
.PHONY: postgres createdb dropdb migrateup migratedown